package model;

/**
 * Class "DMGReport3.0.model.Damage" contains all information about the damage.
 **/
public class Damage {

    private String date;
    private String issue;
    private String description;
    private Room room;
    private User user;
    private DamageStatus Status = DamageStatus.not_done;

    /**
     * Registers the information about the User.
     *
     * @param  date,issue,description,room,user, DMGReport.Damage damage entry  parameters to fill in the information.
     **/
    public Damage(String date, String issue, String description, Room room, User user){
        this.date = date;
        this.issue = issue;
        this.description = description;
        this.room = room;
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DamageStatus getStatus() {
        return Status;
    }

    public void setDamageStatus(Damage.DamageStatus Status) {
        this.Status = Status;
    }

    public enum DamageStatus {Done, not_done}
}
