package model;
import java.util.ArrayList;
import java.util.List;

/**
 * Class "DMGReport3.0.model.User" contains all information about the user.
 **/
public class User {

    private List<Damage> reportedDamages = new ArrayList<>();

    private String fname;
    private String lname;
    private String email;
    private String secretQuestion;
    private String secretAnswer;
    private String password;
    private String birthday;
    private String matricNumber;
    private String points;
    private int repD;

    /**
     * Registers the information about the User.
     *
     * @param fname,lname,email,secretAnswer,secretQuestion,birthday,matricNumber,password,points, DMGReport.User user  parameters to fill in the information.
     **/
    public User(String fname, String lname, String email,String secretAnswer, String secretQuestion, String birthday, String matricNumber, String password, String points){
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.secretAnswer = secretAnswer;
        this.secretQuestion = secretQuestion;
        this.birthday = birthday;
        this.matricNumber = matricNumber;
        this.password = password;
        this.points = points;
    }

    /**
     * Setter and getter for the objects.
     **/
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getR() {
        return repD;
    }

    public void setR(int repD) {
        this.repD = repD;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMatricNumber() {
        return matricNumber;
    }

    public void setMatricNumber(String matricNumber) {
        this.matricNumber = matricNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public List<Damage> getReportedDamages(){
        return reportedDamages;
    }

    public void setReportedDamages(List<Damage> reportedDamages){
        this.reportedDamages = reportedDamages;
    }

    public void addNewDamageUser(Damage newDamage) {
        this.reportedDamages.add(newDamage);
    }
}
