package model;
import java.util.ArrayList;
import java.util.List;

/**
 * Class "DMGReport3.0.model.Room" contains all information about the room.
 **/
public class Room {

    private List<Damage> roomDamages = new ArrayList();
    private int house;
    private int stage;
    private int room;

    /**
     * Registers the information about the User.
     *
     * @param  house,stage,room, DMGReport.Room room  parameters to fill in the information.
     **/
    public Room(int house, int stage, int room) {
        this.house = house;
        this.stage = stage;
        this.room = room;
    }

    /**
     * Setter and getter for the objects.
     **/
    public List<Damage> getRoomDamages(){return roomDamages;};
    public int getHouse(){ return house;};
    public int getStage(){ return stage;};
    public int getRoom() { return room;};

    public void setHouse(int house){this.house = house;};
    public void setStage(int stage){this.stage = stage;};
    public void setRoom(int room){this.room = room;};

    public void addNewDamageRoom(Damage newDamage) {this.roomDamages.add(newDamage);}

}
