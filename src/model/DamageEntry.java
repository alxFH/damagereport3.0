package model;

/**
 * Class "DMGReport3.0.model.DamageEntry" contains all information about the damages in a list.
 **/
public class DamageEntry {
    private String matric;
    private String room;
    private String priority;
    private String status;
    private String issue;
    private String dateR;
    private String description;
    private int dID;

    /**
     * Registers the information about the User.
     *
     * @param  dID,matric,room,priority,status,issue,dateR,description, DMGReport.DamageEntry damage entry  parameters to fill in the information.
     **/
    public DamageEntry(int dID, String matric, String room, String priority, String status, String issue, String dateR,String description){
        this.dID = dID;
        this.matric = matric;
        this.room = room;
        this.priority = priority;
        this.status = status;
        this.issue = issue;
        this.dateR = dateR;
        this.description = description;
    }

    /**
     * Setter and getter for the objects.
     **/
    public String getMatric() {
        return matric;
    }

    public String getRoom() {
        return room;
    }

    public String getPriority() {
        return priority;
    }

    public String getStatus() {
        return status;
    }

    public String getIssue() {
        return issue;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return dateR;
    }

    public void setDate(String date){
        this.dateR = date;
    }

    public int getID(){
        return dID;
    }

}


