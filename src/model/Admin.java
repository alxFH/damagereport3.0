package model;

/**
 * Class "DMGReport3.0.model.Admin" contains all information about the admin.
 **/
public class Admin {

    private String fname;
    private String lname;
    private String password;
    private String loginName;

    /**
     * Registers the information about the User.
     *
     * @param fname,lname,password,loginName, DMGReport.Admin admin parameters to fill in the information.
     **/
    public Admin(String fname, String lname, String password, String loginName){
        this.fname = fname;
        this.lname = lname;
        this.password = password;
        this.loginName = loginName;
    }

    /**
     * Setter and getter for the objects.
     **/
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }


}
