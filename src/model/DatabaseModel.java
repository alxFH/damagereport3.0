package model;
import controller.DataController;
import dbUtil.dbConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.sql.*;

/**
 * Class "DMGReport3.0.model.DatabaseModel" contains all information about the database functions.
 **/
public class DatabaseModel {
    Connection connection;
    /**
     * Generate a connection with database.
     **/
    public DatabaseModel(){
        try{
            this.connection = dbConnector.getConnection();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        if(this.connection == null){
            System.exit(1);
        }
    }
    /**
     * Check connection with database.
     **/
    public boolean isDatabaseConnected(){
        return this.connection != null;
    }
    /**
     * Login user.
     * @param matric,password, parameters to check user login
     **/
    public boolean isLogin(String matric,String password)throws IOException{
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "SELECT * from user where matric = ? and password = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,matric);
            pr.setString(2,password);

            rs = pr.executeQuery();

            if(rs.next()){
                DataController.curentUser = setCurrentUser(rs);

                rs.close();
                pr.close();
                return true;
            }

        } catch(SQLException ex) {
            System.out.println(ex);
            System.out.println("false login data");
            return false;
        }
        return false;
    }
    /**
     * Login admin.
     * @param username,password, parameters to check admin login
     **/
    public boolean isAdminLogin(String username,String password)throws IOException{
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "SELECT * from admin where username = ? and password = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,username);
            pr.setString(2,password);

            rs = pr.executeQuery();

            if(rs.next()){
                rs.close();
                pr.close();
                return true;
            }

        } catch(SQLException ex) {
            System.out.println("false login admin data");
            return false;
        }
        return false;
    }
    /**
     * Set current user in DataController.
     * @param rs, parameters to set current user.
     **/
    public User setCurrentUser(ResultSet rs)throws  SQLException{
        String fname = rs.getString("fname");
        String lname = rs.getString("lname");
        String email = rs.getString("email");
        String secretA = rs.getString("secretA");
        String secretQ = rs.getString("secretQ");
        String birthday = rs.getString("birthday");
        String matric = rs.getString("matric");
        String password = rs.getString("password");
        String points = rs.getString("points");

        User user = new User(fname,lname,email,secretA,secretQ,birthday,matric,password,points);
        rs.close();
        return user;
    }
    /**
     * Register new user in database.
     * @param fname,lname,email,secretQ,secretA,password,birthday,matric,points, parameters to register user
     **/
    public boolean registerUser(String fname,String lname, String email,String secretQ, String secretA,String password, String birthday,String matric, String points){
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql =    "INSERT INTO user (fname, lname, email,secretA,secretQ,birthday,password,matric,points)" +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,fname);
            pr.setString(2,lname);
            pr.setString(3,email);
            pr.setString(4,secretQ);
            pr.setString(5,secretA);
            pr.setString(6,password);
            pr.setString(7,birthday);
            pr.setString(8,matric);
            pr.setString(9,points);

            pr.execute();
            pr.close();
        } catch(SQLException ex) {
            System.out.println("failed register user in database");
            return false;
        }
        return true;
    }
    /**
     * Register new admin in database.
     * @param admin, parameters to register admin
     **/
    public boolean registerAdmin(Admin admin){
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql =    "INSERT INTO admin (username, password, fname, lname)" +
                        "VALUES (?,?,?,?)";
        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,admin.getLoginName());
            pr.setString(2,admin.getPassword());
            pr.setString(3,admin.getFname());
            pr.setString(4,admin.getLname());

            pr.execute();
            pr.close();
        } catch(SQLException ex) {
            System.out.println("failed register admin in database");
            return false;
        }
        return true;
    }
    /**
     * Register new admin in database.
     * @param email,matric,parameters to check register data (already existing email or matric number)
     **/
    public boolean checkRegister(String email, String matric){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "SELECT * from user where matric = ? or email = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,matric);
            pr.setString(2,email);

            rs = pr.executeQuery();

            if(rs.next()){
                rs.close();
                pr.close();
                return true;
            }

        } catch(SQLException ex) {
            System.out.println("false register check data");
            return false;
        }
        return false;
    }
    /**
     * Check email if it already exist.
     * @param email, parameters to check email
     **/
    public boolean checkEmail(String email){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "SELECT * from user where  email = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,email);

            rs = pr.executeQuery();

            if(rs.next()){
                rs.close();
                pr.close();
                return true;
            }

        } catch(SQLException ex) {
            System.out.println("false email search");
            return false;
        }
        return false;
    }
    /**
     * Update user data in database.
     * @param fname,lname,birthday,email, parameters to update user data
     **/
    public boolean updateUser(String fname,String lname, String birthday, String email){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "UPDATE user SET fname = ?, lname = ? , birthday = ?, email = ? WHERE matric = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,fname);
            pr.setString(2,lname);
            pr.setString(3,birthday);
            pr.setString(4,email);
            pr.setString(5,DataController.curentUser.getMatricNumber());

            pr.executeUpdate();

            //rs.close();
            pr.close();

            return true;

        } catch(SQLException ex) {
            System.out.println("update userdata failed!");
            return false;
        }
    }
    /**
     * Update user password in database.
     * @param password, parameters to update user password
     **/
    public boolean updateUserPassword(String password){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "UPDATE user SET password = ? WHERE matric = ?";

        try{
            pr = this.connection.prepareStatement(sql);
            pr.setString(1,password);
            pr.setString(2,DataController.curentUser.getMatricNumber());

            pr.executeUpdate();

            pr.close();
            rs.close();
            return true;

        } catch(SQLException ex) {
            System.out.println("update password failed!");
            return false;
        }
    }
    /**
     * Add new damage to database.
     * @param damage, parameters to add damage to database
     **/
    public boolean addDamage(Damage damage){
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql =    "INSERT INTO damage ( room_id, user_id, date, issue, description, status, priority)" +
                        "VALUES (?,?,?,?,?,?,?)";
        try{
            pr = this.connection.prepareStatement(sql);

            if(!checkRoom(damage)){
                addRoom(damage);
            }

            pr.setInt(1,findRoom(damage));
            pr.setString(2,DataController.curentUser.getMatricNumber());
            pr.setString(3,damage.getDate());
            pr.setString(4,damage.getIssue());
            pr.setString(5,damage.getDescription());
            pr.setString(6,"in progress");
            pr.setString(7, "notSet");


            pr.execute();
            pr.close();


        } catch(SQLException ex) {
            System.out.println(ex);
            System.out.println("failed INSERT INTO damage");
            return false;
        }
        return true;
    }
    /**
     * Update damage data in database.
     * @param damageid, parameters to find damage
     **/
    public boolean updateDamage(int damageid){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sql = "UPDATE damage SET status = ? WHERE id = ?";
        try{
            pr = this.connection.prepareStatement(sql);

            pr.setString(1,"done");
            pr.setInt(2,damageid);

            pr.execute();
            pr.close();


        } catch(SQLException ex) {
            System.out.println(ex);
            System.out.println("failed INSERT INTO damage");
            return false;
        }
        return true;
    }
    /**
     * Add new room to database.
     * @param damage, parameters to add room to database
     **/
    public boolean addRoom(Damage damage){
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql =    "INSERT INTO room (house, stage, room)" +
                        "VALUES (?,?,?)";
        try{
            pr = this.connection.prepareStatement(sql);
            pr.setInt(1,damage.getRoom().getHouse());
            pr.setInt(2,damage.getRoom().getStage());
            pr.setInt(3,damage.getRoom().getRoom());

            pr.execute();
            //rs.close();
            //pr.close();

        } catch(SQLException ex) {
            System.out.println("failed add room");
            System.out.println(ex);
            return false;
        }
        return true;
    }
    /**
     * Check room if already exist.
     * @param damage, parameters to check existing room
     **/
    public boolean checkRoom(Damage damage){
        PreparedStatement pr = null;
        ResultSet rsCheck = null;

        String sqlCheck = "SELECT * FROM room WHERE house = ? and stage = ? and room = ?";
        try{

            pr = this.connection.prepareStatement(sqlCheck);
            pr.setInt(1,damage.getRoom().getHouse());
            pr.setInt(2,damage.getRoom().getStage());
            pr.setInt(3,damage.getRoom().getRoom());

            rsCheck = pr.executeQuery();

            if(rsCheck.next()){
                rsCheck.close();
                pr.close();
                return true;
            }

        } catch(SQLException ex){
            System.out.println("failed check room");
            return false;
        }
        return false;
    }
    /**
     * Search room in database.
     * @param damage, parameters to find room in database
     **/
    public int findRoom(Damage damage){
        PreparedStatement pr = null;
        ResultSet rs = null;
        int roomFound = 0;

        String sqlCheck = "SELECT * FROM room WHERE house = ? and stage = ? and room = ?";
        try{

            pr = this.connection.prepareStatement(sqlCheck);
            pr.setInt(1,damage.getRoom().getHouse());
            pr.setInt(2,damage.getRoom().getStage());
            pr.setInt(3,damage.getRoom().getRoom());

            rs = pr.executeQuery();

            if(rs.next()){
                roomFound =  rs.getInt("id");
                pr.close();
                rs.close();
                return roomFound;
            } else {
                return roomFound;
            }

        } catch(SQLException ex){
            System.out.println(ex);
            System.out.println("find room failed (room_id)");
            return roomFound;
        }
    }
    /**
     * Generate a observable list to fill a table view.
     **/
    public ObservableList<User> getAllUser(){

        ObservableList<User> allUser = FXCollections.observableArrayList();
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sqlCheck = "SELECT * FROM user";
        try{
            pr = this.connection.prepareStatement(sqlCheck);
            rs = pr.executeQuery();

            while(rs.next()){
                String fname = rs.getString("fname");
                String lname = rs.getString("lname");
                String email = rs.getString("email");
                String secretA = rs.getString("secretA");
                String secretQ = rs.getString("secretQ");
                String birthday = rs.getString("birthday");
                String matric = rs.getString("matric");
                String password = rs.getString("password");
                String points = rs.getString("points");
                User user = new User(fname,lname,email,secretQ,secretA,password,matric,birthday,points);

                allUser.add(user);
            }

            pr.close();
            rs.close();

            return allUser;
        } catch(SQLException ex){
            System.out.println("fill all users failed!");
        }
        return allUser;
    }
    /**
     * Generate a observable list to fill a table view.
     **/
    public ObservableList<DamageEntry> getAllDamage(){
        ObservableList<DamageEntry> allDamage = FXCollections.observableArrayList();
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sqlCheck = "SELECT * FROM damage";
        try{
            pr = this.connection.prepareStatement(sqlCheck);
            rs = pr.executeQuery();

            while(rs.next()){
                int id = rs.getInt("id");
                String date = rs.getString("date");
                String issue = rs.getString("issue");
                String userID = Integer.toString(rs.getInt("user_id"));
                String description = rs.getString("description");
                String status = rs.getString("status");
                int roomID = rs.getInt("room_id");

                Room room = findRoom(roomID);
                String buildRoomNumber = Integer.toString(room.getHouse())+"."+Integer.toString(room.getStage())+"."+Integer.toString(room.getRoom());

                DamageEntry damage = new DamageEntry(id,userID,buildRoomNumber,"priority",status,issue,date,description);
                allDamage.add(damage);

            }

            pr.close();
            rs.close();

            return allDamage;

        } catch(SQLException ex){
            System.out.println(ex);
            System.out.println("fill all damages failed!");
        }
        return allDamage;
    }
    /**
     * Count damages of one user.
     * @param matric, parameters to find user damages in database.
     **/
    public int countDamagesUser(String matric){
        PreparedStatement pr = null;
        ResultSet rs = null;
        int repD = 0;

        String sqlCheck = "SELECT * FROM damage WHERE user_id = ?";
        try{
            pr = this.connection.prepareStatement(sqlCheck);
            pr.setString(1, DataController.curentUser.getMatricNumber());
            rs = pr.executeQuery();

            while(rs.next()){
                repD++;
            }
            pr.close();
            rs.close();
            return repD;

        } catch(SQLException ex){
            System.out.println(ex);
            System.out.println("fill user damages failed!");
        }
        return repD;
    }
    /**
     *  Generate a observable list to fill a table view.
     * @param matric, parameters to find user damages in database.
     **/
    public ObservableList<DamageEntry> getUserDamage(String matric){
        ObservableList<DamageEntry> allDamage = FXCollections.observableArrayList();
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sqlCheck = "SELECT * FROM damage WHERE user_id = ?";
        try{
            pr = this.connection.prepareStatement(sqlCheck);
            pr.setString(1, DataController.curentUser.getMatricNumber());
            rs = pr.executeQuery();
            int repD = 0;

            while(rs.next()){
                int id = rs.getInt("id");
                String date = rs.getString("date");
                String description = rs.getString("description");
                String issue = rs.getString("issue");
                String userID = Integer.toString(rs.getInt("user_id"));
                int roomID = rs.getInt("room_id");

                String status = rs.getString("status");

                Room room = findRoom(roomID);
                String buildRoomNumber = Integer.toString(room.getHouse())+"."+Integer.toString(room.getStage())+"."+Integer.toString(room.getRoom());
                DamageEntry damage = new DamageEntry(id,userID,buildRoomNumber,"priority",status,issue,date,description);
                allDamage.add(damage);
                repD++;
            }
            DataController.curentUser.setR(repD);
            pr.close();
            rs.close();
            return allDamage;

        } catch(SQLException ex){
            System.out.println(ex);
            System.out.println("fill user damages failed!");
        }
        return allDamage;
    }
    /**
     * Find room in database.
     * @param damageID, parameters to find room in database.
     **/
    public Room findRoom(int damageID){
        PreparedStatement pr = null;
        ResultSet rs = null;

        String sqlCheck = "SELECT * FROM room WHERE id = ?";
        try{
            pr = this.connection.prepareStatement(sqlCheck);
            pr.setInt(1,damageID);
            rs = pr.executeQuery();

            if(rs.next()){
                int house = rs.getInt("house");
                int stage = rs.getInt("stage");
                int room = rs.getInt("room");

                Room roomDamage = new Room(house,stage,room);

                rs.close();
                pr.close();
                return roomDamage;
            }
        } catch(SQLException ex){
            System.out.println(ex);
            System.out.println("find room (damageTable) failed!");
        }
        return new Room(0,0,0);
    }
}
