import controller.DataController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.application.Application;

/**
 * Created by Alex on 26.06.2017.
 */
public class Main extends Application {
    public static void main(String[] args) throws IOException {

        System.out.println("Start Application");
        DataController DC = new DataController();

        /**
        String path = "C:\\Users\\Alex\\Desktop\\a\\a";
        Runtime rt = Runtime.getRuntime();
        rt.exec("cmd.exe /c cd \""+ path +"\" & start cmd.exe");
        **/

        Application.launch(args);

    }

    @Override
    public void start(Stage s) throws Exception {
        Pane mainPane = (Pane) FXMLLoader.load(Main.class.getResource("view/userMode.fxml"));
        s.setScene(new Scene(mainPane));
        s.setTitle("Damage Report 2.0");
        s.show();
    }
}

