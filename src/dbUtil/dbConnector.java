package dbUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class "DMGReport3.0.dbUtil.dbConnector" contains all functions connect to database.
 **/
public class dbConnector {

    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String CONN = "jdbc:mysql://localhost/login";
    private static final String SQCONN = "jdbc:sqlite:damagereportDB.sqlite";
    /**
     * build up the connection with database.
     **/
    public static Connection getConnection()throws SQLException {

        try{
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(SQCONN);
        }catch(ClassNotFoundException ex){
            ex.printStackTrace();
        }
        return null;
    }

}
