package controller;
import model.DatabaseModel;

/**
 * Created by Alex on 28.06.2017.
 */
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.Damage;
import model.Room;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for reporting damages by users.
 **/
public class ReportControllerView {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TextArea descriptionDamage;

    @FXML
    private TextField categoryDamage;

    @FXML
    private TextField houseNumber;

    @FXML
    private TextField stageNumber;

    @FXML
    private TextField roomNumber;

    @FXML
    private MenuButton priorityDamage;

    @FXML
    private Button reportDamage;

    @FXML
    private Button reportStop;
    /**
     * report damage by user.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void reportDamage(ActionEvent event)throws IOException {
        String formattedDate = "1970.1.1";
        int filledField = 0;

        if(houseNumber.getText().length() > filledField && stageNumber.getText().length() > filledField && roomNumber.getText().length() > filledField && categoryDamage.getText().length() > filledField && descriptionDamage.getText().length() > filledField){
            Room room = new Room(Integer.parseInt(houseNumber.getText()),Integer.parseInt(stageNumber.getText()),Integer.parseInt(roomNumber.getText()));
            Date nd = new Date();
            formattedDate = new SimpleDateFormat("yyyy.MM.dd HH:mm").format(nd);
            Damage damage = new Damage(formattedDate,categoryDamage.getText(),descriptionDamage.getText(),room, DataController.curentUser);
            DBM.addDamage(damage);
            SC.switchScene(event,"profileWindow.fxml");
        } else {
            SC.displayAlert("Fehler!","Alle Felder müssen ausgefüllt sein!");
        }

    }
    /**
     * go back to profile view user.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void reportStop(ActionEvent event)throws IOException{
        SC.switchScene(event,"profileWindow.fxml");
    }
}


