package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for switching between scenes and open alert windows.
 **/
public class SceneController {
    /**
     * switch the scenes in the application.
     * @param event,filename, parameters to get the given event and the filename for new xml.
     **/
    public  boolean switchScene(ActionEvent event, String filename) throws IOException {
        boolean ret=false;
        String cFilename="view/"+filename;
        Parent nextScene = (Parent) FXMLLoader.load(getClass().getClassLoader().getResource(cFilename));
        Scene scene = new Scene(nextScene);

        Node node = (Node)event.getSource();
        Stage sStage =  (Stage) node.getScene().getWindow();

        sStage.setScene(scene);
        sStage.show();

        ret=true;

        return ret;
    }
    /**
     * open an alert window.
     * @param title,message, parameters to fill title and message for alert window.
     **/
    public void displayAlert(String title,String message){
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(350);
        window.setMinHeight(150);

        Label label = new Label();
        label.setText(message);

        Button closeButton = new Button("Ok");
        closeButton.setOnAction(e -> window.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label,closeButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }
}
