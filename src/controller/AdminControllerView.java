package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import model.DamageEntry;
import model.DatabaseModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for click on admin start view.
 **/
public class AdminControllerView implements Initializable {

    DatabaseModel DBM = new DatabaseModel();
    SceneController SC = new SceneController();

    @FXML
    private Text nameAdmin;

    @FXML
    private Button showStudents;

    @FXML
    private Button editDamages;

    @FXML
    private Button createAdmin;

    @FXML
    private Button logoutAdmin;

    @FXML
    private TableView<DamageEntry> damageTable;

    @FXML
    private TableColumn<DamageEntry, String> cStudent;

    @FXML
    private TableColumn<DamageEntry, String> cRoom;

    @FXML
    private TableColumn<DamageEntry, String> cCategory;

    @FXML
    private TableColumn<DamageEntry, String> cPriority;

    @FXML
    private TableColumn<DamageEntry, String> cDate;

    @FXML
    private TableColumn<DamageEntry, String> cStatus;

    /**
     * initialize the view with databse values.
     * @param url, rb, parameters to initialize the view first time.
     **/
    public void initialize(URL url, ResourceBundle rb){

        nameAdmin.setText("Hausmeister");

        cStudent.setCellValueFactory(new PropertyValueFactory<>("matric"));
        cRoom.setCellValueFactory(new PropertyValueFactory<>("room"));
        cCategory.setCellValueFactory(new PropertyValueFactory<>("issue"));
        cPriority.setCellValueFactory(new PropertyValueFactory<>("priority"));
        cDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        cStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

        damageTable.setItems(DBM.getAllDamage());
    }

    /**
     * create admin account.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void createAdmin(ActionEvent event)throws IOException {
        SC.switchScene(event,"addAdminWindow.fxml");
    }
    /**
     * edit damage status to done.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void editDamages(ActionEvent event) {
        DamageEntry d = damageTable.getSelectionModel().getSelectedItem();
        DBM.updateDamage(d.getID());

        cStudent.setCellValueFactory(new PropertyValueFactory<>("matric"));
        cRoom.setCellValueFactory(new PropertyValueFactory<>("room"));
        cCategory.setCellValueFactory(new PropertyValueFactory<>("issue"));
        cPriority.setCellValueFactory(new PropertyValueFactory<>("priority"));
        cDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        cStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

        damageTable.setItems(DBM.getAllDamage());
    }

    /**
     * change view to StudentTableWindow.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void showStudents(ActionEvent event)throws  IOException {
        SC.switchScene(event, "studentTableWindow.fxml");
    }
    /**
     * logout the admin.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void logoutAdmin(ActionEvent event)throws IOException {
        SC.switchScene(event, "userMode.fxml");
        DataController.curentUser = null;
    }

}
