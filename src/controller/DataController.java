package controller;
import model.Damage;
import model.Room;
import model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for logged in user.
 **/
public class DataController {

    public static User curentUser;
    public static List<User> allUsers;
    public static List<Damage> allDamages;
    public static List<Room> allRooms;
    public static boolean admin;

    public DataController(){
        this.admin = false;
        this.curentUser = null;
        this.allUsers = new ArrayList<>();
        this.allDamages = new ArrayList<>();
        this.allRooms = new ArrayList<>();
    }
    /**
     * check two given passwords(equal).
     * @param password1,password2, parameters to check the password repeat clause.
     **/
    public static boolean checkPassword(String password1, String password2){
        if(password1.equals(password2)){
            return true;
        }else {
         return false;
        }
    }
}
