package controller;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

import model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions user login.
 **/
public class LoginControllerView implements Initializable {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TextField passwordUser;

    @FXML
    private Button loginButton;

    @FXML
    private Button registerUser;

    @FXML
    private TextField matricNumber;
    /**
     * check for database connection.
     * @param url, rb, parameters to initialize the view first time.
     **/
    public void initialize(URL url, ResourceBundle rb){
        if(this.DBM.isDatabaseConnected()){
            System.out.println("DB connected!");
        } else {
            System.out.println("DB connect failed!");
        }
    }
    /**
     * go to register view for users.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void registerUser(ActionEvent event)throws IOException {
        SC.switchScene(event, "registerWindow.fxml");
    }
    /**
     * login the admin.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void loginUser(ActionEvent event) throws IOException{
        if(matricNumber.getText().length() > 0 && passwordUser.getText().length() > 0) {
            if (DBM.isLogin(matricNumber.getText(),passwordUser.getText())) {
                DBM.getAllUser();
                SC.switchScene(event, "profileWindow.fxml");
            } else {
                SC.displayAlert("Ungültige Daten!", "Anmeldedaten nicht gefunden!");
            }
        } else {
            SC.displayAlert("Fehler!", "Bitte füllen Sie alle Felder!");
        }
    }
}
