package controller;
import model.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import model.DatabaseModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for startpage after login.
 **/
public class ProfileControllerView implements Initializable {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private Text userName;

    @FXML
    private Text birthdayProfile;

    @FXML
    private Text damagesProfile;

    @FXML
    private Text pointsProfile;

    @FXML
    private Text rankProfile;

    @FXML
    private Button reportDamage;

    @FXML
    private Button changeProfile;

    @FXML
    private Button myDamages;

    @FXML
    private Text damageNumber;

    @FXML
    private Text damageKategory;

    @FXML
    private Text damageDescription;

    @FXML
    private Text damageStatus;

    @FXML
    private Button logoutUser;

    @FXML
    private TableView<DamageEntry> userTable;

    @FXML
    private TableColumn<DamageEntry, String> numberTable;

    @FXML
    private TableColumn<DamageEntry, String> categoryTable;

    @FXML
    private TableColumn<DamageEntry, String> descTable;

    @FXML
    private TableColumn<DamageEntry, String> statusTable;

    /**
     * initialize the view with databse values.
     * @param url,rb, parameters to initialize the view first time.
     **/
    @FXML
    public void initialize(URL url, ResourceBundle rb){
        this.userName.setText(DataController.curentUser.getFname()+" "+DataController.curentUser.getLname());
        this.birthdayProfile.setText(DataController.curentUser.getMatricNumber());
        this.damagesProfile.setText(Integer.toString(DBM.countDamagesUser(DataController.curentUser.getMatricNumber())));
        this.pointsProfile.setText(DataController.curentUser.getPoints());
        this.rankProfile.setText("1");

        numberTable.setCellValueFactory(new PropertyValueFactory<>("room"));
        categoryTable.setCellValueFactory(new PropertyValueFactory<>("issue"));
        descTable.setCellValueFactory(new PropertyValueFactory<>("description"));
        statusTable.setCellValueFactory(new PropertyValueFactory<>("status"));


        userTable.setItems(DBM.getUserDamage(DataController.curentUser.getMatricNumber()));
     }
    /**
     * go to change profile view for user.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void changeProfile(ActionEvent event)throws IOException {
        SC.switchScene(event,"editProfileWindow.fxml");
    }
    /**
     * go to report damage view.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void reportDamage(ActionEvent event)throws IOException {
        SC.switchScene(event,"reportDamage.fxml");
    }
    /**
     * logout the current user.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void logoutUser(ActionEvent event)throws IOException {
        SC.switchScene(event, "userMode.fxml");
        DataController.curentUser = null;
    }

}



