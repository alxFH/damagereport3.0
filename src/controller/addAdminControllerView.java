package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.Admin;
import model.DatabaseModel;

import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.addAdminControllerView" contains all functions for click on create admin view.
 **/
public class addAdminControllerView {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TextField adminName;

    @FXML
    private TextField adminFname;

    @FXML
    private TextField adminLname;

    @FXML
    private PasswordField passwordAdmin;

    @FXML
    private PasswordField  passwordRepeatAdmin;

    @FXML
    private Button addAdmin;

    @FXML
    private Button stopAdmin;

    /**
     * adds an admin to the database.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void addAdmin(ActionEvent event)throws IOException {
        int filledField = 0;
        if(adminFname.getText().length() > filledField && adminLname.getText().length() > filledField && passwordAdmin.getText().length() > filledField && passwordRepeatAdmin.getText().length() > filledField && adminName.getText().length() > filledField){
            if(passwordAdmin.getText().equals(passwordRepeatAdmin.getText())){
                Admin newA = new Admin(adminFname.getText(),adminLname.getText(),passwordAdmin.getText(),adminName.getText());
                DBM.registerAdmin(newA);
                SC.switchScene(event,"adminWindow.fxml");
            } else {
                SC.displayAlert("Fehler!","Passwörter stimmen nicht überein!");
            }
        } else {
            SC.displayAlert("Fehler!","Alle Felder müssen ausgefüllt sein!");
        }
    }
    /**
     * go back to the last view.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void stopAdmin(ActionEvent event)throws IOException {
        SC.switchScene(event,"adminWindow.fxml");
    }
}
