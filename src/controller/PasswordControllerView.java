package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import model.DatabaseModel;

import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for change password.
 **/
public class PasswordControllerView {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private PasswordField currentlyPassword;

    @FXML
    private PasswordField newPassword;

    @FXML
    private PasswordField newPassword1;

    @FXML
    private Button passwordDone;

    @FXML
    private Button passwordStop;
    /**
     * change the user password.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void passwordDone(ActionEvent event)throws IOException{
        int neededPasswordLength = 4;
        if(currentlyPassword.getText().equals(DataController.curentUser.getPassword()) ) {
            if (newPassword.getText().equals(newPassword1.getText()) && newPassword.getText().length() >= neededPasswordLength) {
                if (DBM.updateUserPassword(newPassword.getText())) {
                    SC.switchScene(event, "profileWindow.fxml");
                }
            } else {
                SC.displayAlert("Fehler", "Die Passwörter müssen übereinstimmen und mindesten 4 Zeichen lang sein!");
            }
        } else {
            SC.displayAlert("Fehler", "Ihr eingegebenes aktuelles Passwort ist falsch!");
        }

    }
    /**
     * go back to profile view and interrupt password change.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void passwordStop(ActionEvent event)throws IOException{
        SC.switchScene(event,"editProfileWindow.fxml");
    }
}
