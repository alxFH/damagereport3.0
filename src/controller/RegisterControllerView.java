package controller;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.DatabaseModel;

import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for register a user.
 **/
public class RegisterControllerView {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TextField setLName;

    @FXML
    private TextField setFName;

    @FXML
    private TextField setBirthday;

    @FXML
    private TextField setMatric;

    @FXML
    private TextField setEmail;

    @FXML
    private TextField setSecretQ;

    @FXML
    private TextField setSecretA;

    @FXML
    private TextField setPassword;

    @FXML
    private TextField repeatPassword;

    @FXML
    private Button registerDone;

    @FXML
    private Button registerStop;
    /**
     * register user and fill the database with ne the user data.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void registerDone(ActionEvent event)throws IOException {

        String lname = setLName.getText();
        String fname = setFName.getText();
        String email = setEmail.getText();
        String secretA = setSecretA.getText();
        String secretQ = setSecretQ.getText();
        String birthday = setBirthday.getText();
        String password = setPassword.getText();

        String matric = setMatric.getText();
        String points = "0";

        int registerFail;
        if(!DataController.checkPassword(setPassword.getText(),repeatPassword.getText())){
           registerFail = 1;
        } else if(DBM.checkRegister(setEmail.getText(),matric)){
            registerFail = 2;
        } else if(DBM.registerUser(fname,lname,email,secretA,secretQ,birthday,password,matric,points)){
            registerFail = 0;
        } else {
            registerFail = 3;
        }

        switch(registerFail){
            case 0: SC.switchScene(event,"loginWindow.fxml");
                    break;
            case 1: SC.displayAlert("Fehler!","Passwörter müssen übereinstimmen!");
                    break;
            case 2: SC.displayAlert("Fehler!","Email oder Matrikelnummer bereits vorhanden!");
                    break;
            case 3: SC.displayAlert("Fehler!","Datenbank nicht erreichbar!");
                    break;
            default:break;
        }
    }
    /**
     * go back to login view.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void registerStop(ActionEvent event)throws IOException {
        SC.switchScene(event,"loginWindow.fxml");
    }

}
