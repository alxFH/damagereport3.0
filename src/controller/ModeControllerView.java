package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for admin/student view (user mode).
 **/
public class ModeControllerView {

    SceneController SC = new SceneController();

    @FXML
    private Button adminMode;

    @FXML
    private Button studentMode;
    /**
     * go to login admin window.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void adminMode(ActionEvent event)throws IOException{
        DataController.admin = true;
        SC.switchScene(event, "loginAdminWindow.fxml");
    }
    /**
     * go to login view for users.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void studentMode(ActionEvent event)throws IOException {
        DataController.admin = false;
        SC.switchScene(event, "loginWindow.fxml");
    }

}
