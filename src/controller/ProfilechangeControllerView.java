package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.DatabaseModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions for change user profile.
 **/
public class ProfilechangeControllerView implements Initializable {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TextField changeName;

    @FXML
    private TextField changeName1;

    @FXML
    private TextField changeBirthday;

    @FXML
    private TextField changeEmail;

    @FXML
    private Text changePassword;

    @FXML
    private Button saveProfile;

    @FXML
    private Button btnChangePassword;

    @FXML
    private Button profileStop;
    /**
     * initialize the view with databse values.
     * @param url,rb, parameters to initialize the view first time.
     **/
    @FXML
    public void initialize(URL url, ResourceBundle rb){
        this.changeName.setText(DataController.curentUser.getFname());
        this.changeName1.setText(DataController.curentUser.getLname());
        this.changeBirthday.setText(DataController.curentUser.getBirthday());
        this.changeEmail.setText(DataController.curentUser.getEmail());
    }
    /**
     * go to change password view.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void btnChangePassword(ActionEvent event)throws IOException{
        SC.switchScene(event,"editPassword.fxml");
    }
    /**
     * go back to profile view for users.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void profileStop(ActionEvent event)throws IOException{
        SC.switchScene(event,"profileWindow.fxml");
    }
    /**
     * save the user profile changes in database.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void saveProfile(ActionEvent event)throws IOException {

        String lname = changeName.getText();
        String fname = changeName1.getText();
        String email = changeEmail.getText();
        String birthday = changeBirthday.getText();

        boolean updateAllow = true;
        if(!lname.equals("") && !fname.equals("")&& !email.equals("")&& !birthday.equals("")){
            if(!email.equals(DataController.curentUser.getEmail())){
                if(DBM.checkEmail(email)){
                    SC.displayAlert("Fehler!","Email bereits vorhanden!");
                    updateAllow = false;
                }
            }
        } else {
            SC.displayAlert("Fehler!","Bitte füllen Sie alle Felder aus!");
            updateAllow = false;
        }


        if((DBM.updateUser(fname,lname,birthday,email) && updateAllow)){
            DataController.curentUser.setFname(fname);
            DataController.curentUser.setLname(lname);
            DataController.curentUser.setEmail(email);
            DataController.curentUser.setBirthday(birthday);
            SC.switchScene(event,"profileWindow.fxml");
            System.out.println("Update done!");
        }
    }

}
