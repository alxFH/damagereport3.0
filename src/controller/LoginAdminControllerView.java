package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import model.DatabaseModel;

import java.io.IOException;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions admin login.
 **/
public class LoginAdminControllerView {

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private Pane loginPane;

    @FXML
    private TextField matricNumber;

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField passwordUser;

    @FXML
    private Button backToUserMode;
    /**
     * go back to user mode configuration.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void backToUserMode(ActionEvent event)throws IOException {
        SC.switchScene(event,"userMode.fxml");
    }
    /**
     * login the user to the application.
     * @param event, parameter to react on the specific event.
     **/
    @FXML
    void loginUser(ActionEvent event) throws IOException{
        if(matricNumber.getText().length() > 0 && passwordUser.getText().length() > 0) {
            if (DBM.isAdminLogin(matricNumber.getText(),passwordUser.getText())) {
                DBM.getAllUser();
                SC.switchScene(event, "adminWindow.fxml");
            } else {
                SC.displayAlert("Ungültige Daten!", "Anmeldedaten nicht gefunden!");
            }
        } else {
            SC.displayAlert("Fehler!", "Bitte füllen Sie alle Felder!");
        }
    }

}
