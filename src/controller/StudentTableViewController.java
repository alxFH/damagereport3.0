package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.DatabaseModel;
import model.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class "DMGReport3.0.controller.AdminControllerView" contains all functions to display all students.
 **/
public class StudentTableViewController implements Initializable{

    SceneController SC = new SceneController();
    DatabaseModel DBM = new DatabaseModel();

    @FXML
    private TableView<User> studentTable;

    @FXML
    private TableColumn<User, String> matricNumber;

    @FXML
    private TableColumn<User, String> fname;

    @FXML
    private TableColumn<User, String> lname;

    @FXML
    private TableColumn<User, String> email;

    @FXML
    private TableColumn<User, String> points;

    @FXML
    private Button deleteStudent;

    @FXML
    private Button backButton;

    /**
     * initialize the view with databse values.
     * @param url,rb, parameters to initialize the view first time.
     **/
    @FXML
    public void initialize(URL url, ResourceBundle rb){

        matricNumber.setCellValueFactory(new PropertyValueFactory<>("matricNumber"));
        fname.setCellValueFactory(new PropertyValueFactory<>("fname"));
        lname.setCellValueFactory(new PropertyValueFactory<>("lname"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        points.setCellValueFactory(new PropertyValueFactory<>("points"));

        studentTable.setItems(DBM.getAllUser());
    }
    /**
     * go to the admin view.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void backButton(ActionEvent event)throws IOException {
        SC.switchScene(event, "adminWindow.fxml");
    }
    /**
     * delete students from database.
     * @param event, parameter to get the given event.
     **/
    @FXML
    void deleteStudent(ActionEvent event) {

    }

}
